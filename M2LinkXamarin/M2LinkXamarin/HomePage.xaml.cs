﻿using M2LinkXamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            var user = Application.Current.Properties["user"] as UserModel;
            this.labelWelcome.Text = "Bienvenue "+user.first_name+" "+user.last_name;
		}
	}
}