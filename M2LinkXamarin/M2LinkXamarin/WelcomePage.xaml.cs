﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            loginButton.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new LoginPage());
            };

            registerButton.Clicked += async (s, e) =>
            {
                await Navigation.PushAsync(new RegisterPage());
            };
        }
    }
}