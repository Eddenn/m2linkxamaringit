﻿using M2LinkXamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void Connect(object sender, EventArgs e)
        {
            try
            {
                UserModel user = WSHelper.WSLoginClient.Validate(this.login.Text, this.password.Text);
                String message;
                if(user == null)
                {
                    message = "Login/Mot de passe incorrect.";
                    DisplayAlert("Login", message, "Ok");
                } else
                {
                    Application.Current.Properties.Add("user", user);
                    //var user = Application.Current.Properties["user"] as UserModel;
                    Navigation.PushAsync(new HomePage());
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Login", ex.Message, "Ok");
            }
        }
    }
}