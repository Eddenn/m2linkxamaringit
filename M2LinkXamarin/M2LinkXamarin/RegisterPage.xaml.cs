﻿using M2LinkXamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void Register(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new LoginPage());
            }
            catch (Exception ex)
            {
                DisplayAlert("Register", ex.Message, "Ok");
            }
        }
    }
}