﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace M2LinkXamarin.WebServiceClients
{
    class WSHelper
    {
        public static WSLoginClient WSLoginClient = new WSLoginClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSLogin.svc")
        );

        public static WSMessageClient WSMessageClient = new WSMessageClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSMessage.svc")
        );
    }
}
